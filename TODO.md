# TODO

- composer
- elastic
- java
- logrotate
- MySQL
- nginx
- nginx-config
- nodejs
- ntp
- php
- php-versions
- postfix
- projects
- redis
- sudoers
- users

- CRON

VMs:

- m2-app1 111
  - HDD: 30 GB
  - CPU: 8
  - RAM: 16384 Mb

- m2-app2 112
  - HDD: 30 GB
  - CPU: 8
  - RAM: 16384 Mb

- m2-admin 113
  - HDD: 30 GB
  - CPU: 4
  - RAM: 16384 Mb

- m2-redis 114
  - HDD: 10 GB
  - CPU: 2
  - RAM: 4096 Mb

- vue - 115
  - HDD: 10 GB
  - CPU: 4
  - RAM: 8192 Mb

- vue-redis 116
  - HDD: 10 GB
  - CPU: 2
  - RAM: 4096 Mb

- db - 117
  - HDD: 30 GB
  - CPU: 16
  - RAM: 32768 Mb

- elk - 118
  - HDD: 30 GB
  - CPU: 4
  - RAM: 16384 Mb

- rmq - 119
  - HDD: 10 GB
  - CPU: 2
  - RAM: 4096 Mb

- m2-redis-session 120
  - HDD: 10 GB
  - CPU: 2
  - RAM: 4096 Mb

in /etc/elasticsearch/elasticsearch.yml
added string http.host: 0.0.0.0

для монтирования директории, на сервере использовать команду:
sudo pct set 112 -mp0 /var/www/media,mp=/var/www/utires.com/shared/pub/media
где 112 - ID контейнера
